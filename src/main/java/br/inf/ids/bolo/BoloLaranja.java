package br.inf.ids.bolo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoloLaranja implements Bolo{

    private String descricao;
    @Override
    public void preparar() {
        this.descricao = "Laranja assar, adicionar cobertura.";
    }

}
