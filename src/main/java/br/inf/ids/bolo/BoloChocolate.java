package br.inf.ids.bolo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoloChocolate implements Bolo{

    private String descricao;

    @Override
    public void preparar() {
        this.descricao = "Chocolate, assar, rechear, adicionar cobertura.";
    }

}
