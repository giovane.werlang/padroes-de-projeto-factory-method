package br.inf.ids.bolo;

public interface Bolo {
    void preparar();
    String getDescricao();
}
