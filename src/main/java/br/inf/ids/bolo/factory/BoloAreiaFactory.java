package br.inf.ids.bolo.factory;

import br.inf.ids.bolo.Bolo;
import br.inf.ids.bolo.BoloAreia;

public class BoloAreiaFactory extends BoloFactory {

    @Override
    public Bolo criar() {
        return new BoloAreia();
    }
}
