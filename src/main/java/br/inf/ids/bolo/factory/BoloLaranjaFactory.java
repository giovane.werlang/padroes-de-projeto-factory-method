package br.inf.ids.bolo.factory;

import br.inf.ids.bolo.Bolo;
import br.inf.ids.bolo.BoloLaranja;

public class BoloLaranjaFactory extends BoloFactory {

    @Override
    public Bolo criar() {
        return new BoloLaranja();
    }
}
