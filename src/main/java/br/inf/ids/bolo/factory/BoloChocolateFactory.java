package br.inf.ids.bolo.factory;

import br.inf.ids.bolo.Bolo;
import br.inf.ids.bolo.BoloChocolate;

public class BoloChocolateFactory extends BoloFactory {

    @Override
    public Bolo criar() {
        return new BoloChocolate();
    }
}
