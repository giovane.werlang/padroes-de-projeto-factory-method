package br.inf.ids.bolo.factory;

import br.inf.ids.bolo.Bolo;

public abstract class BoloFactory {

    public String preparar;

    public abstract Bolo criar();
}
