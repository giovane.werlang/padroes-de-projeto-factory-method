package br.inf.ids.bolo;

import br.inf.ids.bolo.enums.TipoBolo;
import br.inf.ids.bolo.factory.BoloAreiaFactory;
import br.inf.ids.bolo.factory.BoloChocolateFactory;
import br.inf.ids.bolo.factory.BoloLaranjaFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/bolo")
public class BoloService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String bolo(@QueryParam("tipo") String tipo) {
        Bolo bolo;
        if(tipo == null || tipo.isEmpty()){
            return null;
        }
        switch (TipoBolo.valueOf(tipo)){
            case AREIA:{
                bolo = new BoloAreiaFactory().criar();
                bolo.preparar();
                return bolo.getDescricao();
            }
            case CHOCOLATE:{
                bolo = new BoloChocolateFactory().criar();
                bolo.preparar();
                return bolo.getDescricao();
            }
            case LARANJA:{
                bolo = new BoloLaranjaFactory().criar();
                bolo.preparar();
                return bolo.getDescricao();
            }
            default:{
                return null;
            }
        }
    }

}
