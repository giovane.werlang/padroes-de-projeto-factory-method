package br.inf.ids.importacao;

import br.inf.ids.importacao.enums.TipoImportacao;
import br.inf.ids.importacao.factory.ImportacaoFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/importar")
public class ImportacaoService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String hello(@QueryParam("tipo") String tipo) {
        ImportacaoFactory factory = new ImportacaoFactory();
        Importacao importacao = factory.getImportacao(TipoImportacao.valueOf(tipo));
        return importacao.importar();
    }

}