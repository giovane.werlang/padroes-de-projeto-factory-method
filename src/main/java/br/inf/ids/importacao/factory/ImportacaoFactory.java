package br.inf.ids.importacao.factory;

import br.inf.ids.importacao.ImportaCenso;
import br.inf.ids.importacao.ImportaSere;
import br.inf.ids.importacao.Importacao;
import br.inf.ids.importacao.enums.TipoImportacao;

public class ImportacaoFactory {

    public Importacao getImportacao(TipoImportacao tipo){
        if(tipo == null){
            return null;
        }
        switch (tipo){
            case SERE:{
                return new ImportaSere();
            }
            case CENSO:{
                return new ImportaCenso();
            }
            default:{
                return null;
            }
        }
    }
}
